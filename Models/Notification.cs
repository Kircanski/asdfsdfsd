﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Models
{
    public class Notification
    {

        [ForeignKey("DeviceGroupId")]
        public int DeviceGroupId { get; set; }
        [ForeignKey("DeviceId")]
        public int DeviceId { get; set; }
        

        [Key]
        public int Id { get; set; }

        public string Message { get; set; }

        public string NotificationType { get; set; }

        public string DateTime { get; set; }

        public string NotificationScope { get; set; }

      
    }

}


