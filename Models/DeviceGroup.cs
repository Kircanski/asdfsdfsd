﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Models
{
    public class DeviceGroup

    {


        public int DeviceGroupId { get; set; }

        public string DeviceGroupName { get; set; }

        public ICollection<Notification> Notifications { get; set; }    
    }
}
    