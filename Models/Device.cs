﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Models
{
    public class Device
    {
        
        public int DeviceId { get; set; }   

        public string DeviceName { get; set; }

        public ICollection<Notification> Notifications { get; set; }
    }
}   
    