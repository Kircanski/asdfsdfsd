﻿using Microsoft.EntityFrameworkCore;
using NotificationCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Data
{
    public class NotificationContext : DbContext
    {
        public NotificationContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }

        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Device> Devices { get; set; }

        public DbSet<DeviceGroup> DevicesGroup { get; set; } 

    }
}
