﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using NotificationCenter.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Data
{
    public class DummyData
    {
        public static void Initialize(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<NotificationContext>();
                context.Database.EnsureCreated();

                if (context.Notifications != null && context.Notifications.Any())
                    return;
                var notifications = GetNotifications().ToArray();
                context.Notifications.AddRange(notifications);
                context.SaveChanges();

                var devices = GetDevices(context).ToArray();
                context.Devices.AddRange(devices);
                context.SaveChanges();

                var devicesGroup = GetDevicesGroup(context).ToArray();
                context.DevicesGroup.AddRange(devicesGroup);
                context.SaveChanges();

                
            }
        }
        public static List<Notification> GetNotifications()
        {
            List<Notification> notifications = new List<Notification>() {
            new Notification {NotificationType = "Urgent" , Message ="notifications is urgent" , DateTime = "12.07. at 15:04" , NotificationScope = "Specific"  } ,
            new Notification {NotificationType = "Warning" , Message ="Warning!" , DateTime = "12.07. at 15:05" , NotificationScope = "Specific" } ,
            new Notification {NotificationType = "Info" , Message ="Info" , DateTime = "12.07. at 15:06" , NotificationScope = "Device"  } ,
            new Notification {NotificationType = "Warning" , Message ="Warning!" , DateTime = "12.07. at 15:07" , NotificationScope = "Group" } ,
            new Notification {NotificationType = "Info" , Message ="Info" , DateTime = "12.07. at 15:08" , NotificationScope = "Global"  } ,
        };
            return notifications;
        }

        public static List<Device> GetDevices(NotificationContext db)
        {
            List<Device> devices = new List<Device>()
            {
                new Device
                {
                    DeviceName = "Device1",

                    Notifications = new List<Notification>(db.Notifications.Take(1))
                },
                new Device
                {
                    DeviceName = "Device2",

                    Notifications = new List<Notification>(db.Notifications.Take(2))
                },
                new Device
                {
                    DeviceName = "Device3",

                    Notifications = new List<Notification>(db.Notifications.Take(3))
                },
                new Device
                {
                    DeviceName = "Device4",

                    Notifications = new List<Notification>(db.Notifications.Take(4))
                },
                new Device
                {
                    DeviceName = "Device5",
                    
                    
                    Notifications = new List<Notification>(db.Notifications.Take(5))
                },
            };
            return devices;
        }

        public static List<DeviceGroup> GetDevicesGroup(NotificationContext db)
            {

            List<DeviceGroup> devicesGroup = new List<DeviceGroup>()
            {
                new DeviceGroup
                {
                    DeviceGroupName = "Device1",
                    
                    Notifications = new List<Notification>(db.Notifications.Take(1))
                },
                new DeviceGroup
                {
                    DeviceGroupName = "Device2",
                    
                    Notifications = new List<Notification>(db.Notifications.Take(2))
                },
                new DeviceGroup
                {
                    DeviceGroupName = "Device3",
                    
                    Notifications = new List<Notification>(db.Notifications.Take(3))
                },
                new DeviceGroup
                {
                    DeviceGroupName = "Device4",
                    
                    Notifications = new List<Notification>(db.Notifications.Take(4))
                },
                new DeviceGroup
                {
                    DeviceGroupName = "Device5",
                   
                    Notifications = new List<Notification>(db.Notifications.Take(5))
                },
            };
                return devicesGroup;
            }
        }
    }
