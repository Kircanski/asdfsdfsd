﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotificationCenter.Data;
using NotificationCenter.Models;
using NotificationCenter.Services;

namespace NotificationCenter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DevicesController : ControllerBase
    {
        private readonly IDeviceService _deviceService;

        public DevicesController(IDeviceService deviceService)
        {
            _deviceService = deviceService;
        }

        // GET: api/Devices
        [HttpGet]
        public  IEnumerable GetAll ()
        {
            return _deviceService.GetAll();

        }

        // GET: api/Devices/1/Notification
        [HttpGet("{id:int}/notification")]

        public  ActionResult<Device> Include (int id)
        {
            Device device = _deviceService.Include(id);
            if (device == null)
                return NotFound(); 
            return Ok(device.Notifications);
        }
        // GET: api/Devices/5
        [HttpGet("{id}")]
        public ActionResult<Device> GetAll  ([FromRoute]int id)
        {
            var device = _deviceService.GetById(id);

            // var device2 =await _context.Devices.FindAsync(id)

            if (device == null)
            {
                return NotFound();
            }

            return Ok(device);
        }

        // PUT: api/Devices/5
        [HttpPut("{id}")]
        public ActionResult<Device> Update (int id, [FromBody] Device deviceUpdate)
        {
            if (deviceUpdate == null)
            {
                return BadRequest();
            }

            var existingDeviceItem = _deviceService.GetById(id);

            if (existingDeviceItem == null)
            {
                return NotFound();
            }
            return _deviceService.Update(id,deviceUpdate);
            
        }

           
        // POST: api/Devices
        [HttpPost]
        public  ActionResult<Device> Create (Device device)
        {
            var existingDeviceItem = _deviceService.GetById(device.DeviceId);
            if  (existingDeviceItem != null)
            {
                return BadRequest();
            }
            _deviceService.Create(device);

            if (device == null)
            {
                return NotFound();
            }


            return device;
        }

        // DELETE: api/Devices/5
        [HttpDelete("{id}")]
        public ActionResult  Delete(int id)
        {
            var device = _deviceService.GetById(id);
            if (device == null)
            {
                return NotFound();
            }

             _deviceService.Delete(id);
            
            return Ok(device);
        }

      
    }
}
