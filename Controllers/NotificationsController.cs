﻿
using System.Collections;
using Microsoft.AspNetCore.Mvc;
using NotificationCenter.Models;
using NotificationCenter.Services;

namespace NotificationCenter.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly INotificationService _notificationService;

        public NotificationsController(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }

        // GET: api/Notifications
        [HttpGet]
        public IEnumerable GetAll()
        {
            return _notificationService.GetAll();

        }

        // GET: api/Notifications/5
        [HttpGet("{id}")]
        public ActionResult<Notification> GetById(int id)
        {
            var notification = _notificationService.GetById(id);

            if (notification == null)
            {
                return NotFound();
            }

            return notification;
        }

        // PUT: api/Notifications/5
        [HttpPut("{id}")]
        public ActionResult<Notification> PutNotification(int id, [FromBody] Notification notificationUpdate)
        {
            if (notificationUpdate == null)
            {
                return BadRequest();
            }

            var existingNotification = _notificationService.GetById(id);
            
            if (existingNotification == null)
            {
                return NotFound();
            }
         

            return _notificationService.Update(id,notificationUpdate);
        }

        // POST: api/Notifications
        [HttpPost]
        public ActionResult<Notification> PostNotification(Notification notification)


        {

            // if notification == null -> badrequest
            // if existingNotification != null -> bad request
            var existingNotificationItem = _notificationService.GetById(notification.Id);
            if (existingNotificationItem != null)
            {
                return BadRequest();
            }
            _notificationService.Create(notification);

            if (notification == null)
            {
                return NotFound();
            }


            return Ok(notification);
        }

        // DELETE: api/Notifications/5
        [HttpDelete("{id}")]
        public ActionResult<Notification> DeleteNotification(int id)
        {
            var notification = _notificationService.GetById(id);
            if (notification == null)   
            {
                return NotFound();
            }

            _notificationService.Delete(id);

            return null;
        }
  
    }
}
