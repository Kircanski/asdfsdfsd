﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotificationCenter.Data;
using NotificationCenter.Models;

namespace NotificationCenter.Services
{
    public class NotificationService : INotificationService

    {
        private readonly NotificationContext _context;

        public  NotificationService(NotificationContext context)
        {
            _context = context;
        }


        public Notification Create(Notification notification)
        {
            _context.Notifications.Add(notification);
            _context.SaveChanges();


            return notification;
            
        }



        public void Delete(int id)
        {
            Notification notification = GetById(id);
            _context.Notifications.Remove(notification);
            _context.SaveChanges();


        }



        public Notification GetById(int id)
        {
            return  _context.Notifications.FirstOrDefault(x => x.Id == id);
        }



        public IEnumerable<Notification> GetAll()
        {
            return _context.Notifications.ToList();
        }



        public Notification Update(int id, Notification notification)
            {
            _context.Notifications.Update(notification);
            _context.SaveChanges();
            return notification;
        }

        
    }
}

