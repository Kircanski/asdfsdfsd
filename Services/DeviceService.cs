﻿using Microsoft.EntityFrameworkCore;
using NotificationCenter.Data;
using NotificationCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Services
{
    public class DeviceService : IDeviceService
    {
        private readonly NotificationContext _context;

        public DeviceService(NotificationContext context)
        {
            _context = context;
        }
            

        public Device Create(Device device)
        {
            _context.Devices.Add(device);
            _context.SaveChanges();
            return device;
      

        }



        public void Delete(int id)
        {
            Device device = GetById(id);
            _context.Devices.Remove(device);
            _context.SaveChanges();

        }



        public Device GetById(int id)
        {
            return _context.Devices.FirstOrDefault(y => y.DeviceId == id);
            
        }



        public IEnumerable<Device> GetAll()
        {
           return _context.Devices.ToList();
        }



        public Device Update(int id , Device device)
        {
            _context.Devices.Update(device);
            _context.SaveChanges();
            return device;
        }

        public Device Include (int id)
        {
            return _context.Devices.Include(n => n.Notifications).FirstOrDefault(i => i.DeviceId == id);
        }
        

        public bool Save()
        {
            return (_context.SaveChanges() >= 0);
        }
    }
}