﻿using NotificationCenter.Models;
using System.Collections.Generic;

namespace NotificationCenter.Services
{
    public interface INotificationService
    {

        IEnumerable<Notification> GetAll();

        Notification GetById(int id);

        Notification Update(int id, Notification notification);

        Notification Create(Notification notification);

        void Delete(int id);
      //  object Entry(Notification notification);
    }
}