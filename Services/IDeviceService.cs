﻿using NotificationCenter.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificationCenter.Services
{
    public interface IDeviceService
    {

        Device Create(Device device);

        Device GetById(int id);

        IEnumerable<Device> GetAll();

        Device Update(int id , Device device);

        void Delete(int id);
        Device Include(int id);


    }
}
